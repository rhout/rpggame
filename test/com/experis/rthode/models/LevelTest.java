package com.experis.rthode.models;

import com.experis.rthode.models.characters.Character;
import com.experis.rthode.models.characters.CharacterFactory;
import com.experis.rthode.models.characters.CharacterType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LevelTest {
    Character character;

    @BeforeEach
    void initAll() {
        character = CharacterFactory.getInstance().getCharacter("Frodo", CharacterType.MAGE);
    }

    @Test
    void checkGainsXpCorectly() {
        assertEquals(character.getCurrentXp(), 0);
        assertEquals(character.getXpToNextLevel(), 100);
        character.gainXp(100);
        assertEquals(character.getCurrentXp(), 100);
        assertEquals(character.getXpToNextLevel(), 110);
        character.gainXp(111);
        assertEquals(character.getCurrentXp(), 211);
        assertEquals(character.getXpToNextLevel(), 331 - 211);
        character.gainXp(500);
        assertEquals(character.getCurrentXp(), 711);
        assertEquals(character.getXpToNextLevel(), 770 - 711);
    }

    @Test
    void checkLevelsUpOn() {
        assertEquals(character.getLevel(), 1);
        character.gainXp(100);
        assertEquals(character.getLevel(), 2);
        character.gainXp(365);
        assertEquals(character.getLevel(), 5);
    }
}