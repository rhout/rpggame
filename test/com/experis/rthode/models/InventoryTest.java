package com.experis.rthode.models;

import com.experis.rthode.models.items.Item;
import com.experis.rthode.models.items.armor.ArmorFactory;
import com.experis.rthode.models.items.armor.BodyType;
import com.experis.rthode.models.items.armor.materials.MaterialType;
import com.experis.rthode.models.items.weapon.WeaponFactory;
import com.experis.rthode.models.items.weapon.WeaponType;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InventoryTest {
    Inventory inventory = new Inventory();
    WeaponFactory weapFactory = WeaponFactory.getInstance();
    ArmorFactory armorFactory = ArmorFactory.getInstance();

    @Test
    void equip() {
        assertNull(inventory.getEquippedWeapon());
        Item weapon = weapFactory.getWeapon("Some Name", 3, WeaponType.MAGIC);
        inventory.equip(weapon);
        assertEquals(weapon, inventory.getEquippedWeapon());

        weapon = weapFactory.getWeapon("Another Name", 4, WeaponType.MELEE);
        inventory.equip(weapon);

        Item headArmor = armorFactory.getArmor("Armor Name", 4, MaterialType.CLOTH, BodyType.HEAD);
        inventory.equip(headArmor);

        assertEquals(weapon, inventory.getEquippedWeapon());
        assertEquals(headArmor, inventory.getEquippedItem(BodyType.HEAD));
        assertNull(inventory.getEquippedItem(BodyType.TORSO));

    }

    @Test
    void calculateAllArmorBonuses() {
        Item head = armorFactory.getArmor("Some name", 2, MaterialType.PLATE, BodyType.HEAD);
        Item legs = armorFactory.getArmor("Some name", 4, MaterialType.CLOTH, BodyType.LEGS);
        Item torso = armorFactory.getArmor("Some name", 6, MaterialType.LEATHER, BodyType.TORSO);
        inventory.equip(head);
        inventory.equip(legs);
        inventory.equip(torso);
        Attributes calculatedAttributes = inventory.calculateAllArmorBonuses();
        assertEquals((int) (54 * 0.8 + 0.6 * 30 + 68), calculatedAttributes.health);
        assertEquals((int) (7 * 0.8 + 7), calculatedAttributes.strength);
        assertEquals((int) (3 * 0.8 + 0.6 * 5 + 15), calculatedAttributes.dexterity);
        assertEquals((int) (0.6 * 11), calculatedAttributes.intelligence);
    }

    @Test
    void removesOldAttributesWhenEquippingNewItem() {
        Item legs = armorFactory.getArmor("Some name", 4, MaterialType.CLOTH, BodyType.LEGS);
        inventory.equip(legs);
        Attributes calculatedAttributes = inventory.calculateAllArmorBonuses();
        assertEquals((int) (0.6 * 30), calculatedAttributes.health);
        assertEquals(0, calculatedAttributes.strength);
        assertEquals((int) (0.6 * 5), calculatedAttributes.dexterity);
        assertEquals((int) (0.6 * 11), calculatedAttributes.intelligence);

        inventory.equip(legs);
        inventory.equip(legs);
        inventory.equip(legs);
        calculatedAttributes = inventory.calculateAllArmorBonuses();
        assertEquals((int) (0.6 * 30), calculatedAttributes.health);
        assertEquals(0, calculatedAttributes.strength);
        assertEquals((int) (0.6 * 5), calculatedAttributes.dexterity);
        assertEquals((int) (0.6 * 11), calculatedAttributes.intelligence);
    }

    @Test
    void getEquippedItem() {
        assertNull(inventory.getEquippedWeapon());
        assertNull(inventory.getEquippedItem(BodyType.TORSO));
        assertNull(inventory.getEquippedItem(BodyType.HEAD));
        assertNull(inventory.getEquippedItem(BodyType.LEGS));
        Item weapon = weapFactory.getWeapon("Some Name", 3, WeaponType.MAGIC);
        inventory.equip(weapon);
        assertEquals(weapon, inventory.getEquippedWeapon());
        weapon = weapFactory.getWeapon("Another Name", 4, WeaponType.MELEE);
        inventory.equip(weapon);
        assertEquals(weapon, inventory.getEquippedWeapon());
    }
}