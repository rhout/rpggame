package com.experis.rthode.models.characters;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WarriorTest {
    CharacterFactory factory = CharacterFactory.getInstance();
    Character character;

    @Test
    void calculateBaseAttributesWarrior() {
        character = factory.getCharacter("Name", CharacterType.WARRIOR);
        assertEquals(150, character.calculateBaseAttributes().health);
        assertEquals(10, character.calculateBaseAttributes().strength);
        assertEquals(3, character.calculateBaseAttributes().dexterity);
        assertEquals(1, character.calculateBaseAttributes().intelligence);
        character.gainXp(332);
        assertEquals(150 + 30*3, character.calculateBaseAttributes().health);
        assertEquals(10 + 5*3, character.calculateBaseAttributes().strength);
        assertEquals(3 + 2*3, character.calculateBaseAttributes().dexterity);
        assertEquals(1 + 3, character.calculateBaseAttributes().intelligence);
    }
}