package com.experis.rthode.models.characters;

import com.experis.rthode.models.characters.characterclasses.Mage;
import com.experis.rthode.models.characters.characterclasses.Ranger;
import com.experis.rthode.models.characters.characterclasses.Warrior;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CharacterFactoryTest {
    CharacterFactory factory = CharacterFactory.getInstance();
    Character character;

    @Test
    void createWarrior() {
        String name = "Some cool name";
        character = factory.getCharacter(name, CharacterType.WARRIOR);
        assertEquals(name, character.getName());
        assertTrue(character.getCharacterClass() instanceof Warrior);
        assertFalse(character.getCharacterClass() instanceof Ranger);
    }

    @Test
    void createRanger() {
        String name = "Some cool name";
        character = factory.getCharacter(name, CharacterType.RANGER);
        assertEquals(name, character.getName());
        assertTrue(character.getCharacterClass() instanceof Ranger);
        assertFalse(character.getCharacterClass() instanceof Mage);
    }

    @Test
    void createMage() {
        String name = "Some cool name";
        character = factory.getCharacter(name, CharacterType.MAGE);
        assertEquals(name, character.getName());
        assertTrue(character.getCharacterClass() instanceof Mage);
        assertFalse(character.getCharacterClass() instanceof Ranger);
    }
}