package com.experis.rthode.models.characters;

import com.experis.rthode.models.Attributes;
import com.experis.rthode.models.items.Item;
import com.experis.rthode.models.items.armor.ArmorFactory;
import com.experis.rthode.models.items.armor.BodyType;
import com.experis.rthode.models.items.armor.materials.MaterialType;
import com.experis.rthode.models.items.weapon.WeaponFactory;
import com.experis.rthode.models.items.weapon.WeaponType;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CharacterTest {
    CharacterFactory characterFactory = CharacterFactory.getInstance();
    WeaponFactory weaponFactory = WeaponFactory.getInstance();
    ArmorFactory armorFactory = ArmorFactory.getInstance();
    Character character;

    @Test
    void canEquip() {
        character = characterFactory.getCharacter("Some cool name", CharacterType.WARRIOR);
        Item weapon0 = weaponFactory.getWeapon("Bla", 0, WeaponType.MAGIC);
        Item weapon1 = weaponFactory.getWeapon("Bla", 1, WeaponType.MAGIC);
        Item weapon2 = weaponFactory.getWeapon("Bla", 2, WeaponType.MAGIC);
        assertTrue(character.canEquip(weapon0));
        assertTrue(character.canEquip(weapon1));
        assertFalse(character.canEquip(weapon2));
    }

    @Test
    void equipWeapon() {
        character = characterFactory.getCharacter("Some cool name", CharacterType.WARRIOR);
        Item weapon = weaponFactory.getWeapon("Bla", 1, WeaponType.MAGIC);

        assertEquals(0, character.calculateBonusAttributes().health);
        assertEquals(0, character.calculateBonusAttributes().strength);
        assertEquals(0, character.calculateBonusAttributes().dexterity);
        assertEquals(0, character.calculateBonusAttributes().intelligence);
        character.equipItem(weapon);
        assertEquals(0, character.calculateBonusAttributes().health);
        assertEquals(0, character.calculateBonusAttributes().strength);
        assertEquals(0, character.calculateBonusAttributes().dexterity);
        assertEquals(0, character.calculateBonusAttributes().intelligence);

        assertEquals(weapon, character.inventory.getEquippedItem(BodyType.HAND));
    }

    @Test
    void equipArmor() {
        character = characterFactory.getCharacter("Some cool name", CharacterType.WARRIOR);
        Item armor = armorFactory.getArmor("Hyp", 1, MaterialType.CLOTH, BodyType.TORSO);
        assertNull(character.inventory.getEquippedItem(BodyType.TORSO));
        assertEquals(0, character.calculateBonusAttributes().health);
        assertEquals(0, character.calculateBonusAttributes().strength);
        assertEquals(0, character.calculateBonusAttributes().dexterity);
        assertEquals(0, character.calculateBonusAttributes().intelligence);
        character.equipItem(armor);
        assertEquals(armor, character.inventory.getEquippedItem(BodyType.TORSO));

        assertEquals(15, character.calculateBonusAttributes().health);
        assertEquals(0, character.calculateBonusAttributes().strength);
        assertEquals(2, character.calculateBonusAttributes().dexterity);
        assertEquals(5, character.calculateBonusAttributes().intelligence);
    }

    @Test
    void totalAttributes() {
        character = characterFactory.getCharacter("Some cool name", CharacterType.WARRIOR);
        Item armor = armorFactory.getArmor("Hyp", 1, MaterialType.CLOTH, BodyType.TORSO);
        character.equipItem(armor);

        Attributes attributesSum = new Attributes(character.calculateBonusAttributes());
        attributesSum.add(character.calculateBaseAttributes());

        assertEquals(165, attributesSum.health);
        assertEquals(10, attributesSum.strength);
        assertEquals(5, attributesSum.dexterity);
        assertEquals(6, attributesSum.intelligence);
    }

    @Test
    void canAttack() {
        character = characterFactory.getCharacter("Some cool name", CharacterType.WARRIOR);
        assertFalse(character.canAttack());
        Item weapon = WeaponFactory.getInstance().getWeapon("Bla", 1, WeaponType.MAGIC);
        character.equipItem(weapon);
        assertTrue(character.canAttack());
    }

    @Test
    void attackWarriorMeleeNaked() {
        character = characterFactory.getCharacter("Some cool name", CharacterType.WARRIOR);
        Item weaponLevel1 = weaponFactory.getWeapon("Bla", 1, WeaponType.MELEE);
        Item weaponLevel4 = weaponFactory.getWeapon("Bla", 4, WeaponType.MELEE);

        assertEquals(0, character.attack());
        character.equipItem(weaponLevel1);
        assertEquals(32, character.attack());

        character.gainXp(332);
        assertEquals(54, character.attack());
        character.equipItem(weaponLevel4);
        assertEquals(60, character.attack());
    }

    @Test
    void attackWarriorMeleeArmored() {
        character = characterFactory.getCharacter("Some cool name", CharacterType.WARRIOR);
        Item weaponLevel1 = weaponFactory.getWeapon("Bla", 1, WeaponType.RANGED);
        Item weaponLevel4 = weaponFactory.getWeapon("Bla", 4, WeaponType.MELEE);
        Item headLevel4 = armorFactory.getArmor("haha", 4, MaterialType.PLATE, BodyType.HEAD);
        Item legsLevel4 = armorFactory.getArmor("haha", 4, MaterialType.LEATHER, BodyType.LEGS);
        Item torsoLevel4 = armorFactory.getArmor("haha", 4, MaterialType.CLOTH, BodyType.TORSO);

        assertEquals(0, character.attack());
        character.equipItem(weaponLevel1);
        assertEquals(14, character.attack());
        character.gainXp(340);
        character.equipItem(weaponLevel4);
        assertEquals(60, character.attack());

        character.equipItem(headLevel4);
        character.equipItem(legsLevel4);
        character.equipItem(torsoLevel4);

        assertEquals(77, character.attack());
    }
}