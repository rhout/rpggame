package com.experis.rthode.models.characters;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MageTest {
    CharacterFactory factory = CharacterFactory.getInstance();
    Character character;

    @Test
    void calculateBaseAttributesMage() {
        character = factory.getCharacter("Name", CharacterType.MAGE);
        assertEquals(100, character.calculateBaseAttributes().health);
        assertEquals(2, character.calculateBaseAttributes().strength);
        assertEquals(3, character.calculateBaseAttributes().dexterity);
        assertEquals(10, character.calculateBaseAttributes().intelligence);
        character.gainXp(732);
        assertEquals(100 + 15*5, character.calculateBaseAttributes().health);
        assertEquals(2 + 5, character.calculateBaseAttributes().strength);
        assertEquals(3 + 2*5, character.calculateBaseAttributes().dexterity);
        assertEquals(10 + 5*5, character.calculateBaseAttributes().intelligence);
    }
}