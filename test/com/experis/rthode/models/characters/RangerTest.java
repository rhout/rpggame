package com.experis.rthode.models.characters;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RangerTest {
    CharacterFactory factory = CharacterFactory.getInstance();
    Character character;

    @Test
    void calculateBaseAttributesRanger() {
        character = factory.getCharacter("Name", CharacterType.RANGER);
        assertEquals(120, character.calculateBaseAttributes().health);
        assertEquals(5, character.calculateBaseAttributes().strength);
        assertEquals(10, character.calculateBaseAttributes().dexterity);
        assertEquals(2, character.calculateBaseAttributes().intelligence);
        character.gainXp(330);
        assertEquals(120 + 20*2, character.calculateBaseAttributes().health);
        assertEquals(5 + 2*2, character.calculateBaseAttributes().strength);
        assertEquals(10 + 5*2, character.calculateBaseAttributes().dexterity);
        assertEquals(2 + 2, character.calculateBaseAttributes().intelligence);
    }
}