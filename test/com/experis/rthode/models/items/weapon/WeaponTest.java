package com.experis.rthode.models.items.weapon;

import com.experis.rthode.models.Attributes;
import com.experis.rthode.models.items.Item;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WeaponTest {
    Item weapon;

    @Test
    void testMeleeWeaponDamage() {
        weapon = WeaponFactory.getInstance().getWeapon("Cool named axe", 1, WeaponType.MELEE);
        Attributes attributes = new Attributes(20, 3, 3, 8);
        assertEquals(((Weapon) weapon).calculateDamageBasedOnAttributes(attributes), 21);
    }

    @Test
    void testMagicWeaponDamage() {
        weapon = WeaponFactory.getInstance().getWeapon("Better named staff", 10, WeaponType.MAGIC);
        Attributes attributes = new Attributes(20, 6, 3, 3);
        assertEquals(((Weapon) weapon).calculateDamageBasedOnAttributes(attributes), 25 + (10 * 2) + 3 * 3);
    }

    @Test
    void testRangedWeaponDamage() {
        weapon = WeaponFactory.getInstance().getWeapon("Decent named bow", 4, WeaponType.RANGED);

        Attributes attributes = new Attributes(20, 6, 3, 8);

        assertEquals(((Weapon) weapon).calculateDamageBasedOnAttributes(attributes), 5 + (4 * 3) + 3 * 2);
    }
}