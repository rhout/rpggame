package com.experis.rthode.models.items.weapon;

import com.experis.rthode.models.items.Item;
import com.experis.rthode.models.items.armor.BodyType;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WeaponFactoryTest {
    WeaponFactory factory = WeaponFactory.getInstance();
    Item weapon;

    @Test
    void getMagicWeapon() {
        weapon = factory.getWeapon("Weapon name", 3, WeaponType.MAGIC);

        assertEquals("Weapon name", weapon.getName());
        assertEquals(3, weapon.getItemLevel());
        assertEquals(BodyType.HAND, weapon.getBodyType());
        assertEquals("Magic", ((Weapon)weapon).getWeaponType());
    }

    @Test
    void getRangedWeapon() {
        weapon = factory.getWeapon("Weapon name", 3, WeaponType.RANGED);

        assertEquals("Weapon name", weapon.getName());
        assertEquals(3, weapon.getItemLevel());
        assertEquals(BodyType.HAND, weapon.getBodyType());
        assertEquals("Ranged", ((Weapon)weapon).getWeaponType());
    }

    @Test
    void getMeleeWeapon() {
        weapon = factory.getWeapon("Weapon name", 3, WeaponType.MELEE);

        assertEquals("Weapon name", weapon.getName());
        assertEquals(3, weapon.getItemLevel());
        assertEquals(BodyType.HAND, weapon.getBodyType());
        assertEquals("Melee", ((Weapon)weapon).getWeaponType());
    }
}