package com.experis.rthode.models.items.armor;

import com.experis.rthode.models.items.Item;
import com.experis.rthode.models.items.armor.materials.Cloth;
import com.experis.rthode.models.items.armor.materials.Leather;
import com.experis.rthode.models.items.armor.materials.MaterialType;
import com.experis.rthode.models.items.armor.materials.Plate;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArmorFactoryTest {
    ArmorFactory armorFactory = ArmorFactory.getInstance();
    Item armor;
    @Test
    void getHeadLeatherArmor() {
        armor = armorFactory.getArmor("Armor name", 3, MaterialType.LEATHER, BodyType.HEAD);

        assertEquals(BodyType.HEAD, armor.getBodyType());
        assertEquals(Leather.class.getSimpleName(), ((Armor)armor).getMaterialType());
        assertEquals(3, armor.getItemLevel());
        assertEquals("Armor name", armor.getName());
    }

    @Test
    void getHeadClothArmor() {
        armor = armorFactory.getArmor("Armor name", 3, MaterialType.CLOTH, BodyType.HEAD);

        assertEquals(BodyType.HEAD, armor.getBodyType());
        assertEquals(Cloth.class.getSimpleName(), ((Armor)armor).getMaterialType());
        assertEquals(3, armor.getItemLevel());
        assertEquals("Armor name", armor.getName());
    }

    @Test
    void getHeadPlateArmor() {
        armor = armorFactory.getArmor("Armor name", 3, MaterialType.PLATE, BodyType.HEAD);

        assertEquals(BodyType.HEAD, armor.getBodyType());
        assertEquals(Plate.class.getSimpleName(), ((Armor)armor).getMaterialType());
        assertEquals(3, armor.getItemLevel());
        assertEquals("Armor name", armor.getName());
    }

    @Test
    void getLegsLeatherArmor() {
        armor = armorFactory.getArmor("Armor name", 3, MaterialType.LEATHER, BodyType.LEGS);

        assertEquals(BodyType.LEGS, armor.getBodyType());
        assertEquals(Leather.class.getSimpleName(), ((Armor)armor).getMaterialType());
        assertEquals(3, armor.getItemLevel());
        assertEquals("Armor name", armor.getName());
    }

    @Test
    void getLegsClothArmor() {
        armor = armorFactory.getArmor("Armor name", 3, MaterialType.CLOTH, BodyType.LEGS);

        assertEquals(BodyType.LEGS, armor.getBodyType());
        assertEquals(Cloth.class.getSimpleName(), ((Armor)armor).getMaterialType());
        assertEquals(3, armor.getItemLevel());
        assertEquals("Armor name", armor.getName());
    }

    @Test
    void getLegsPlateArmor() {
        armor = armorFactory.getArmor("Armor name", 3, MaterialType.PLATE, BodyType.LEGS);

        assertEquals(BodyType.LEGS, armor.getBodyType());
        assertEquals(Plate.class.getSimpleName(), ((Armor)armor).getMaterialType());
        assertEquals(3, armor.getItemLevel());
        assertEquals("Armor name", armor.getName());
    }

    @Test
    void getTorsoLeatherArmor() {
        armor = armorFactory.getArmor("Armor name", 3, MaterialType.LEATHER, BodyType.TORSO);

        assertEquals(BodyType.TORSO, armor.getBodyType());
        assertEquals(Leather.class.getSimpleName(), ((Armor)armor).getMaterialType());
        assertEquals(3, armor.getItemLevel());
        assertEquals("Armor name", armor.getName());
    }

    @Test
    void getTorsoClothArmor() {
        armor = armorFactory.getArmor("Armor name", 3, MaterialType.CLOTH, BodyType.TORSO);

        assertEquals(BodyType.TORSO, armor.getBodyType());
        assertEquals(Cloth.class.getSimpleName(), ((Armor)armor).getMaterialType());
        assertEquals(3, armor.getItemLevel());
        assertEquals("Armor name", armor.getName());
    }

    @Test
    void getTorsoPlateArmor() {
        armor = armorFactory.getArmor("Armor name", 3, MaterialType.PLATE, BodyType.TORSO);

        assertEquals(BodyType.TORSO, armor.getBodyType());
        assertEquals(Plate.class.getSimpleName(), ((Armor)armor).getMaterialType());
        assertEquals(3, armor.getItemLevel());
        assertEquals("Armor name", armor.getName());
    }
}