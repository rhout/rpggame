package com.experis.rthode.models.items.armor.materials;

import com.experis.rthode.models.Attributes;
import com.experis.rthode.models.items.Item;
import com.experis.rthode.models.items.armor.Armor;
import com.experis.rthode.models.items.armor.ArmorFactory;
import com.experis.rthode.models.items.armor.BodyType;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LeatherTest {
    ArmorFactory armorFactory = ArmorFactory.getInstance();
    @Test
    void calculateHeadArmorBonus() {
        Item leatherHead = armorFactory.getArmor("Leather head", 3, MaterialType.LEATHER, BodyType.HEAD);
        assertEquals(BodyType.HEAD, leatherHead.getBodyType());
        assertEquals(3, leatherHead.getItemLevel());
        assertEquals("Leather head", leatherHead.getName());

        Attributes calulatedAttributes = ((Armor) leatherHead).calculateAttributeBonus();
        assertEquals(35, calulatedAttributes.health);
        assertEquals(3, calulatedAttributes.strength);
        assertEquals(7, calulatedAttributes.dexterity);
        assertEquals(0, calulatedAttributes.intelligence);
    }

    @Test
    void calculateLegsArmorBonus() {
        Item leatherHands = armorFactory.getArmor("Leather legs", 5, MaterialType.LEATHER, BodyType.LEGS);

        assertEquals(BodyType.LEGS, leatherHands.getBodyType());
        assertEquals(5, leatherHands.getItemLevel());
        assertEquals("Leather legs", leatherHands.getName());

        Attributes calulatedAttributes = ((Armor) leatherHands).calculateAttributeBonus();
        assertEquals(36, calulatedAttributes.health);
        assertEquals(3, calulatedAttributes.strength);
        assertEquals(7, calulatedAttributes.dexterity);
        assertEquals(0, calulatedAttributes.intelligence);
    }

    @Test
    void calculateTorsoArmorBonus() {
        Item leatherTorso = armorFactory.getArmor("Leather torso", 10, MaterialType.LEATHER, BodyType.TORSO);

        assertEquals(BodyType.TORSO, leatherTorso.getBodyType());
        assertEquals(10, leatherTorso.getItemLevel());
        assertEquals("Leather torso", leatherTorso.getName());

        Attributes calulatedAttributes = ((Armor) leatherTorso).calculateAttributeBonus();
        assertEquals(100, calulatedAttributes.health);
        assertEquals(11, calulatedAttributes.strength);
        assertEquals(23, calulatedAttributes.dexterity);
        assertEquals(0, calulatedAttributes.intelligence);
    }
}