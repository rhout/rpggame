package com.experis.rthode.models.items.armor.materials;

import com.experis.rthode.models.Attributes;
import com.experis.rthode.models.items.Item;
import com.experis.rthode.models.items.armor.Armor;
import com.experis.rthode.models.items.armor.ArmorFactory;
import com.experis.rthode.models.items.armor.BodyType;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ClothTest {
    ArmorFactory armorFactory = ArmorFactory.getInstance();
    @Test
    void calculateHeadArmorBonus() {
        Item clothHead = armorFactory.getArmor("Cloth head", 3, MaterialType.CLOTH, BodyType.HEAD);
        assertEquals(BodyType.HEAD, clothHead.getBodyType());
        assertEquals(3, clothHead.getItemLevel());
        assertEquals("Cloth head", clothHead.getName());

        Attributes calulatedAttributes = ((Armor) clothHead).calculateAttributeBonus();
        assertEquals(20, calulatedAttributes.health);
        assertEquals(0, calulatedAttributes.strength);
        assertEquals(3, calulatedAttributes.dexterity);
        assertEquals(7, calulatedAttributes.intelligence);
    }

    @Test
    void calculateLegsArmorBonus() {
        Item clothHands = armorFactory.getArmor("Cloth legs", 5, MaterialType.CLOTH, BodyType.LEGS);

        assertEquals(BodyType.LEGS, clothHands.getBodyType());
        assertEquals(5, clothHands.getItemLevel());
        assertEquals("Cloth legs", clothHands.getName());

        Attributes calulatedAttributes = ((Armor) clothHands).calculateAttributeBonus();
        assertEquals(21, calulatedAttributes.health);
        assertEquals(0, calulatedAttributes.strength);
        assertEquals(3, calulatedAttributes.dexterity);
        assertEquals(7, calulatedAttributes.intelligence);
    }

    @Test
    void calculateTorsoArmorBonus() {
        Item clothTorso = armorFactory.getArmor("Cloth torso", 10, MaterialType.CLOTH, BodyType.TORSO);

        assertEquals(BodyType.TORSO, clothTorso.getBodyType());
        assertEquals(10, clothTorso.getItemLevel());
        assertEquals("Cloth torso", clothTorso.getName());

        Attributes calulatedAttributes = ((Armor) clothTorso).calculateAttributeBonus();
        assertEquals(60, calulatedAttributes.health);
        assertEquals(0, calulatedAttributes.strength);
        assertEquals(11, calulatedAttributes.dexterity);
        assertEquals(23, calulatedAttributes.intelligence);
    }
}