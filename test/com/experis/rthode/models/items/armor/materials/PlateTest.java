package com.experis.rthode.models.items.armor.materials;

import com.experis.rthode.models.Attributes;
import com.experis.rthode.models.items.Item;
import com.experis.rthode.models.items.armor.Armor;
import com.experis.rthode.models.items.armor.ArmorFactory;
import com.experis.rthode.models.items.armor.BodyType;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlateTest {
    ArmorFactory armorFactory = ArmorFactory.getInstance();
    @Test
    void calculateHeadArmorBonus() {
        Item plateHead = armorFactory.getArmor("Plate head", 3, MaterialType.PLATE, BodyType.HEAD);
        assertEquals(BodyType.HEAD, plateHead.getBodyType());
        assertEquals(3, plateHead.getItemLevel());
        assertEquals("Plate head", plateHead.getName());

        Attributes calulatedAttributes = ((Armor) plateHead).calculateAttributeBonus();
        assertEquals(52, calulatedAttributes.health);
        assertEquals(7, calulatedAttributes.strength);
        assertEquals(3, calulatedAttributes.dexterity);
        assertEquals(0, calulatedAttributes.intelligence);
    }

    @Test
    void calculateLegsArmorBonus() {
        Item plateHands = armorFactory.getArmor("Plate hands", 5, MaterialType.PLATE, BodyType.LEGS);

        assertEquals(BodyType.LEGS, plateHands.getBodyType());
        assertEquals(5, plateHands.getItemLevel());
        assertEquals("Plate hands", plateHands.getName());

        Attributes calulatedAttributes = ((Armor) plateHands).calculateAttributeBonus();
        assertEquals(54, calulatedAttributes.health);
        assertEquals(7, calulatedAttributes.strength);
        assertEquals(3, calulatedAttributes.dexterity);
        assertEquals(0, calulatedAttributes.intelligence);
    }

    @Test
    void calculateTorsoArmorBonus() {
        Item plateTorso = armorFactory.getArmor("Plate torso", 10, MaterialType.PLATE, BodyType.TORSO);

        assertEquals(BodyType.TORSO, plateTorso.getBodyType());
        assertEquals(10, plateTorso.getItemLevel());
        assertEquals("Plate torso", plateTorso.getName());

        Attributes calulatedAttributes = ((Armor) plateTorso).calculateAttributeBonus();
        assertEquals(150, calulatedAttributes.health);
        assertEquals(23, calulatedAttributes.strength);
        assertEquals(11, calulatedAttributes.dexterity);
        assertEquals(0, calulatedAttributes.intelligence);
    }
}