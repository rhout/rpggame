package com.experis.rthode.models.items.armor;

import com.experis.rthode.models.Attributes;
import com.experis.rthode.models.items.Item;
import com.experis.rthode.models.items.armor.materials.Cloth;
import com.experis.rthode.models.items.armor.materials.Material;
import com.experis.rthode.models.items.armor.materials.MaterialType;
import com.experis.rthode.models.items.weapon.WeaponFactory;
import com.experis.rthode.models.items.weapon.WeaponType;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArmorTest {
    Item armor;

    @Test
    void testClothTorsoBonus() {
        armor = ArmorFactory.getInstance().getArmor("Cool named cloth body", 3, MaterialType.CLOTH, BodyType.TORSO);
        Attributes attributes = ((Armor) armor).calculateAttributeBonus();
        assertEquals(25, attributes.health);
        assertEquals(0, attributes.strength);
        assertEquals(4, attributes.dexterity);
        assertEquals(9, attributes.intelligence);
    }

    @Test
    void testClothHeadBonus() {
        armor = ArmorFactory.getInstance().getArmor("Cool named cloth head", 3, MaterialType.CLOTH, BodyType.HEAD);
        Attributes attributes = ((Armor) armor).calculateAttributeBonus();
        assertEquals((int) (25 * 0.8), attributes.health);
        assertEquals(0, attributes.strength);
        assertEquals((int) (4 * 0.8), attributes.dexterity);
        assertEquals((int) (9 * 0.8), attributes.intelligence);
    }

    @Test
    void testClothLegsBonus() {
        armor = ArmorFactory.getInstance().getArmor("Cool named cloth legs", 3, MaterialType.CLOTH, BodyType.LEGS);
        Attributes attributes = ((Armor) armor).calculateAttributeBonus();
        assertEquals((int) (25 * 0.6), attributes.health);
        assertEquals(0, attributes.strength);
        assertEquals((int) (4 * 0.6), attributes.dexterity);
        assertEquals((int) (9 * 0.6), attributes.intelligence);
    }

    @Test
    void testLeatherTorsoBonus() {
        armor = ArmorFactory.getInstance().getArmor("Cool named leather body", 3, MaterialType.LEATHER, BodyType.TORSO);
        Attributes attributes = ((Armor) armor).calculateAttributeBonus();
        assertEquals(44, attributes.health);
        assertEquals(4, attributes.strength);
        assertEquals(9, attributes.dexterity);
        assertEquals(0, attributes.intelligence);
    }

    @Test
    void testLeatherHeadBonus() {
        armor = ArmorFactory.getInstance().getArmor("Cool named leather head", 3, MaterialType.LEATHER, BodyType.HEAD);
        Attributes attributes = ((Armor) armor).calculateAttributeBonus();
        assertEquals((int) (44 * 0.8), attributes.health);
        assertEquals((int) (4 * 0.8), attributes.strength);
        assertEquals((int) (9 * 0.8), attributes.dexterity);
        assertEquals(0, attributes.intelligence);
    }

    @Test
    void testLeatherLegsBonus() {
        armor = ArmorFactory.getInstance().getArmor("Cool named leather legs", 3, MaterialType.LEATHER, BodyType.LEGS);
        Attributes attributes = ((Armor) armor).calculateAttributeBonus();
        assertEquals((int) (44 * 0.6), attributes.health);
        assertEquals((int) (4 * 0.6), attributes.strength);
        assertEquals((int) (9 * 0.6), attributes.dexterity);
        assertEquals(0, attributes.intelligence);
    }

    @Test
    void testPlateTorsoBonus() {
        armor = ArmorFactory.getInstance().getArmor("Cool named plate body", 3, MaterialType.PLATE, BodyType.TORSO);
        Attributes attributes = ((Armor) armor).calculateAttributeBonus();
        assertEquals(66, attributes.health);
        assertEquals(9, attributes.strength);
        assertEquals(4, attributes.dexterity);
        assertEquals(0, attributes.intelligence);
    }

    @Test
    void testPlateHeadBonus() {
        armor = ArmorFactory.getInstance().getArmor("Cool named plate head", 3, MaterialType.PLATE, BodyType.HEAD);
        Attributes attributes = ((Armor) armor).calculateAttributeBonus();
        assertEquals((int) (66 * 0.8), attributes.health);
        assertEquals((int) (9 * 0.8), attributes.strength);
        assertEquals((int) (4 * 0.8), attributes.dexterity);
        assertEquals(0, attributes.intelligence);
    }

    @Test
    void testPlateLegsBonus() {
        armor = ArmorFactory.getInstance().getArmor("Cool named plate legs", 3, MaterialType.PLATE, BodyType.LEGS);
        Attributes attributes = ((Armor) armor).calculateAttributeBonus();
        assertEquals((int) (66 * 0.6), attributes.health);
        assertEquals((int) (9 * 0.6), attributes.strength);
        assertEquals((int) (4 * 0.6), attributes.dexterity);
        assertEquals(0, attributes.intelligence);
    }
}