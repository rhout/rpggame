package com.experis.rthode.models;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AttributesTest {

    @Test
    void add() {
        Attributes attributes = new Attributes(2, 3, 4, 5);
        Attributes attributesToAdd = new Attributes(1, 1, 1, 1);
        attributes.add(attributesToAdd);
        assertEquals(3, attributes.health);
        assertEquals(4, attributes.strength);
        assertEquals(5, attributes.dexterity);
        assertEquals(6, attributes.intelligence);
    }
}