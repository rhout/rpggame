package com.experis.rthode.models;

/**
 * Wrapper for the character level with responsibilities of gaining xp and increasing the level.
 */
public class Level {
    private int currentXp = 0;
    private int targetXp = 100;
    private int level = 1;
    private int previousTargetXpStep = targetXp;

    /**
     * Adds the given xp to the character and updates level accordingly.
     * @param gainedXp
     */
    public void gainXp(int gainedXp) {
        currentXp += gainedXp;
        updateLevelBasedOnCurrentXp();
    }

    /**
     * Increases the level of the character if the current xp is higher than the target xp for the next level.
     */
    private void updateLevelBasedOnCurrentXp() {
        if (targetXp > currentXp) {
            return;
        }
        previousTargetXpStep *= 1.1;
        targetXp += previousTargetXpStep;
        level++;
        updateLevelBasedOnCurrentXp();
    }

    /**
     * CAlculates how much xp is needed to gain a level.
     * @return
     */
    public int xpToNextLevel() {
        return targetXp - currentXp;
    }

    public int getCurrentXp() {
        return currentXp;
    }

    public int getLevel() {
        return level;
    }
}
