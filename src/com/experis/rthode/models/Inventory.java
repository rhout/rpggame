package com.experis.rthode.models;

import com.experis.rthode.models.items.Item;
import com.experis.rthode.models.items.armor.Armor;
import com.experis.rthode.models.items.armor.BodyType;
import com.experis.rthode.models.items.weapon.Weapon;

import java.util.EnumMap;

/**
 * Holds all the items the character has equipped and is responsible for calculationg the total armor bonus.
 */
public class Inventory {
    EnumMap<BodyType, Item> inventory = new EnumMap<>(BodyType.class);

    /**
     * Adds the given item to the inventory in the slot where the item belongs.
     *
     * @param itemToEquip
     */
    public void equip(Item itemToEquip) {
        inventory.put(itemToEquip.getBodyType(), itemToEquip);
    }

    /**
     * Sums all the individual equipped armor bonuses.
     *
     * @return The total attribute bonus.
     */
    public Attributes calculateAllArmorBonuses() {
        Attributes totalBonus = new Attributes(0, 0, 0, 0);
        for (Item item : inventory.values()) {
            if (isArmor(item)) {
                totalBonus.add(((Armor) item).calculateAttributeBonus());
            }
        }
        return totalBonus;
    }

    /**
     * Determines if the specified item is an armor type.
     *
     * @param item
     * @return
     */
    private boolean isArmor(Item item) {
        return item.getBodyType() != BodyType.HAND;
    }

    /**
     * Gets the item that is equipped in the specified body slot.
     *
     * @param bodyType
     * @return
     */
    public Item getEquippedItem(BodyType bodyType) {
        return inventory.get(bodyType);
    }

    /**
     * Gets the equipped weapon.
     *
     * @return
     */
    public Weapon getEquippedWeapon() {
        return (Weapon) getEquippedItem(BodyType.HAND);
    }
}
