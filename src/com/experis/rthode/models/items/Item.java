package com.experis.rthode.models.items;

import com.experis.rthode.models.items.armor.BodyType;

/**
 * The super class for the item types.
 */
public abstract class Item {
    protected String name;
    protected int itemLevel;
    protected BodyType bodyType;

    public Item(String name, int level) {
        this.name = name;
        this.itemLevel = level;
    }

    public String getName() {
        return name;
    }

    public BodyType getBodyType() {
        return bodyType;
    }

    public int getItemLevel() {
        return itemLevel;
    }
}
