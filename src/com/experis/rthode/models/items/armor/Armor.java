package com.experis.rthode.models.items.armor;

import com.experis.rthode.models.Attributes;
import com.experis.rthode.models.items.*;
import com.experis.rthode.models.items.armor.materials.Material;

/**
 * Contain the information of the armor and calculates the attribute bonus gained from the armor.
 */
public class Armor extends Item {
    private Material material;
    
    public Armor(String name, int level, Material material, BodyType bodyType) {
        super(name, level);
        this.material = material;
        this.bodyType = bodyType;
    }

    /**
     * Calculates the attributes this armor gives including the penalty for which body type it is.
     * @return
     */
    public Attributes calculateAttributeBonus() {
        return material.calculateArmorBonus(itemLevel, bodyType);
    }

    /**
     * Gets the name of the material type of the armor.
     * @return
     */
    public String getMaterialType() {
        return material.getClass().getSimpleName();
    }
}
