package com.experis.rthode.models.items.armor.materials;

import com.experis.rthode.models.Attributes;
import com.experis.rthode.models.items.armor.BodyType;

/**
 * Contains the base and scaling attributes for the plate type.
 */
public class Plate implements Material {
    private final int BASE_HEALTH = 30;
    private final int BASE_STRENGTH = 3;
    private final int BASE_DEXTERITY = 1;

    private final int SCALING_HEALTH = 12;
    private final int SCALING_STRENGTH = 2;
    private final int SCALING_DEXTERITY = 1;


    public Attributes calculateArmorBonus(int armorLevel, BodyType bodyType) {
        int healthBonus = (int) ((BASE_HEALTH + armorLevel*SCALING_HEALTH) * bodyType.penalty());
        int strengthBonus = (int) ((BASE_STRENGTH + armorLevel*SCALING_STRENGTH) * bodyType.penalty());
        int dexterityBonus = (int) ((BASE_DEXTERITY + armorLevel*SCALING_DEXTERITY) * bodyType.penalty());
        return new Attributes(healthBonus, strengthBonus, dexterityBonus, 0);
    }
}
