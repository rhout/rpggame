package com.experis.rthode.models.items.armor.materials;

import com.experis.rthode.models.Attributes;
import com.experis.rthode.models.items.armor.BodyType;

public interface Material {
    /**
     * Calculates the attribute bonus an armor piece gives including the slot penalty.
     * @param armorLevel
     * @param bodyType
     * @return The attribute bonus given.
     */
    Attributes calculateArmorBonus(int armorLevel, BodyType bodyType);
}
