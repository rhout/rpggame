package com.experis.rthode.models.items.armor.materials;

import com.experis.rthode.models.Attributes;
import com.experis.rthode.models.items.armor.BodyType;

/**
 * Contains the base and scaling attributes for the cloth type.
 */
public class Cloth implements Material {
    private final int BASE_HEALTH = 10;
    private final int BASE_DEXTERITY = 1;
    private final int BASE_INTELLIGENCE = 3;

    private final int SCALING_HEALTH = 5;
    private final int SCALING_DEXTERITY = 1;
    private final int SCALING_INTELLIGENCE = 2;

    public Attributes calculateArmorBonus(int armorLevel, BodyType bodyType) {
        int healthBonus = (int) ((BASE_HEALTH + armorLevel*SCALING_HEALTH) * bodyType.penalty());
        int dexterityBonus = (int) ((BASE_DEXTERITY + armorLevel*SCALING_DEXTERITY) * bodyType.penalty());
        int intelligenceBonus = (int) ((BASE_INTELLIGENCE + armorLevel*SCALING_INTELLIGENCE) * bodyType.penalty());
        return new Attributes(healthBonus, 0, dexterityBonus, intelligenceBonus);
    }
}
