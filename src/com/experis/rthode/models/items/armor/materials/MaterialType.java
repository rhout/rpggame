package com.experis.rthode.models.items.armor.materials;

/**
 * The types of material an armor can have.
 */
public enum MaterialType {
    CLOTH,
    PLATE,
    LEATHER
}
