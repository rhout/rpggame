package com.experis.rthode.models.items.armor;

import com.experis.rthode.models.items.Item;
import com.experis.rthode.models.items.armor.materials.*;

public class ArmorFactory {
    private static ArmorFactory single_instance = null;

    private ArmorFactory() {
    }

    /**
     * Gets the instance of the singleton armor factory or creates one if it doesn't exist.
     * @return The factory instance.
     */
    public static ArmorFactory getInstance() {
        if (single_instance == null) {
            return new ArmorFactory();
        }
        return single_instance;
    }

    /**
     * Creates an armor object.
     *
     * @param armorName
     * @param armorLevel
     * @param materialType
     * @param bodyType
     * @return
     */
    public Item getArmor(String armorName, int armorLevel, MaterialType materialType, BodyType bodyType) {
        if (materialType == MaterialType.CLOTH) {
            return new Armor(armorName, armorLevel, new Cloth(), bodyType);
        } else if (materialType == MaterialType.LEATHER) {
            return new Armor(armorName, armorLevel, new Leather(), bodyType);
        } else if (materialType == MaterialType.PLATE) {
            return new Armor(armorName, armorLevel, new Plate(), bodyType);
        }
        return null;
    }
}
