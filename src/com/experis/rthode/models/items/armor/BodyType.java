package com.experis.rthode.models.items.armor;

/**
 * The possible slots an item can fit into including the attribute penalty bonus for the armor.
 */
public enum BodyType {
    TORSO {
        public double penalty() {
            return 1;
        }
    },
    LEGS {
        public double penalty() {
            return 0.6;
        }
    },
    HEAD {
        public double penalty() {
            return 0.8;
        }
    },
    HAND {
        public double penalty() {
            return 1;
        }
    };

    /**
     * The penalty the specific armor type gives to the armor attributes.
     * @return
     */
    public abstract double penalty();
}
