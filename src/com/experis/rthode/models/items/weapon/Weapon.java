package com.experis.rthode.models.items.weapon;

import com.experis.rthode.models.Attributes;
import com.experis.rthode.models.items.Item;
import com.experis.rthode.models.items.armor.BodyType;
import com.experis.rthode.models.items.weapon.weapondamage.WeaponTypeDamage;

/**
 * Contain the information of a weapon and calculates the damage done based on the characters attributes.
 */
public class Weapon extends Item {
    WeaponTypeDamage weaponTypeDamage;

    public Weapon(String name, int weaponLevel, WeaponTypeDamage weaponTypeDamage) {
        super(name, weaponLevel);
        this.weaponTypeDamage = weaponTypeDamage;
        this.bodyType = BodyType.HAND;
    }

    public int calculateDamageBasedOnAttributes(Attributes characterAttributes) {
        return weaponTypeDamage.calculateDamage(itemLevel, characterAttributes);
    }


    /**
     * Gets the name of the weappon type. Takes the class name and removed the "Damage" in the name.
     *
     * @return
     */
    public String getWeaponType() {
        String className = weaponTypeDamage.getClass().getSimpleName();
        return className.substring(0, className.indexOf("D"));
    }
}
