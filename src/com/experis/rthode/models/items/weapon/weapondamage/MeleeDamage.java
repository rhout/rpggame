package com.experis.rthode.models.items.weapon.weapondamage;

import com.experis.rthode.models.Attributes;

/**
 * Representation of the melee weapon type.
 */
public class MeleeDamage implements WeaponTypeDamage {

    public int calculateDamage(int weaponLevel, Attributes characterAttributes) {
        int weaponDamage = 15 + weaponLevel * 2;
        int addedBonus = (int) (1.5 * characterAttributes.strength);
        return weaponDamage + addedBonus;
    }
}
