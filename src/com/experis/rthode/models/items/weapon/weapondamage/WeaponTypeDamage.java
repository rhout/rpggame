package com.experis.rthode.models.items.weapon.weapondamage;

import com.experis.rthode.models.Attributes;


public interface WeaponTypeDamage {
    /**
     * Calculates the damage dealt by a weapon based on its type and the characters attributes.
     * @param weaponLevel
     * @param characterAttributes
     * @return Total damage dealt
     */
    int calculateDamage(int weaponLevel, Attributes characterAttributes);
}
