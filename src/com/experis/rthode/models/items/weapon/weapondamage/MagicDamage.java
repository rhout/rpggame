package com.experis.rthode.models.items.weapon.weapondamage;

import com.experis.rthode.models.Attributes;

/**
 * Representation of the magic weapon type.
 */
public class MagicDamage implements WeaponTypeDamage {

    public int calculateDamage(int weaponLevel, Attributes characterAttributes) {
        int weaponDamage = 25 + weaponLevel*2;
        int addedBonus = 3 * characterAttributes.intelligence;
        return weaponDamage + addedBonus;
    }
}
