package com.experis.rthode.models.items.weapon.weapondamage;

import com.experis.rthode.models.Attributes;

/**
 * Representation of the ranged weapon type.
 */
public class RangedDamage implements WeaponTypeDamage {

    public int calculateDamage(int weaponLevel, Attributes characterAttributes) {
        int weaponDamage = 5 + weaponLevel*3;
        int addedBonus = 2 * characterAttributes.dexterity;
        return weaponDamage + addedBonus;
    }
}
