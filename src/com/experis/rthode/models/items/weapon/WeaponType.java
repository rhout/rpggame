package com.experis.rthode.models.items.weapon;

/**
 * Holds the types a weapon can have.
 */
public enum WeaponType {
    MELEE,
    RANGED,
    MAGIC
}
