package com.experis.rthode.models.items.weapon;

import com.experis.rthode.models.items.Item;
import com.experis.rthode.models.items.weapon.weapondamage.MagicDamage;
import com.experis.rthode.models.items.weapon.weapondamage.MeleeDamage;
import com.experis.rthode.models.items.weapon.weapondamage.RangedDamage;

public class WeaponFactory {
    private static WeaponFactory single_instance = null;

    private WeaponFactory() {
    }

    /**
     * Gets the instance of the singleton weapon factory or creates one if it doesn't exist.
     *
     * @return The factory instance.
     */
    public static WeaponFactory getInstance() {
        if (single_instance == null) {
            return new WeaponFactory();
        }
        return single_instance;
    }

    /**
     * Creates a weapon object.
     *
     * @param weaponName
     * @param itemLevel
     * @param weaponType
     * @return
     */
    public Item getWeapon(String weaponName, int itemLevel, WeaponType weaponType) {
        if (weaponType == WeaponType.MELEE) {
            return new Weapon(weaponName, itemLevel, new MeleeDamage());
        } else if (weaponType == WeaponType.RANGED) {
            return new Weapon(weaponName, itemLevel, new RangedDamage());
        } else if (weaponType == WeaponType.MAGIC) {
            return new Weapon(weaponName, itemLevel, new MagicDamage());
        }
        return null;
    }
}
