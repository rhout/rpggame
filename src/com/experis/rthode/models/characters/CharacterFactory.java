package com.experis.rthode.models.characters;

import com.experis.rthode.models.characters.characterclasses.Mage;
import com.experis.rthode.models.characters.characterclasses.Ranger;
import com.experis.rthode.models.characters.characterclasses.Warrior;

public class CharacterFactory {
    private static CharacterFactory single_instance = null;

    private CharacterFactory() {}

    /**
     * Gets the instance of the singleton character factory or creates one if it doesn't exist.
     * @return The factory instance.
     */
    public static CharacterFactory getInstance() {
        if (single_instance == null) {
            return new CharacterFactory();
        }
        return single_instance;
    }

    /**
     * Creates a character object.
     * @param characterName
     * @param characterType From the valid character classes as seen in {@link CharacterType}
     * @return
     */
    public Character getCharacter(String characterName, CharacterType characterType) {
        if (characterType == CharacterType.MAGE) {
            return new Character(characterName, new Mage());
        } else if (characterType == CharacterType.RANGER) {
            return new Character(characterName, new Ranger());
        } else if (characterType == CharacterType.WARRIOR) {
            return new Character(characterName, new Warrior());
        }

        return null;
    }
}
