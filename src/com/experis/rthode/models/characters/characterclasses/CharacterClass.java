package com.experis.rthode.models.characters.characterclasses;

import com.experis.rthode.models.Attributes;

public interface CharacterClass {
    /**
     * Calculates a characters attributes based on the base attributes and the character level.
     *
     * @param level of the character
     * @return The attributes for the character at the specified level
     */
    Attributes calculateBaseAttributes(int level);
}
