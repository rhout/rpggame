package com.experis.rthode.models.characters.characterclasses;

import com.experis.rthode.models.Attributes;

/**
 * Representation of the mage class.
 */
public class Warrior implements CharacterClass {
    private final int BASE_HEALTH = 150;
    private final int BASE_STRENGTH = 10;
    private final int BASE_DEXTERITY = 3;
    private final int BASE_INTELLIGENCE = 1;

    private final int SCALING_HEALTH = 30;
    private final int SCALING_STRENGTH = 5;
    private final int SCALING_DEXTERITY = 2;
    private final int SCALING_INTELLIGENCE = 1;

    public Attributes calculateBaseAttributes(int level) {
        int health = BASE_HEALTH + SCALING_HEALTH * (level - 1);
        int strength = BASE_STRENGTH + SCALING_STRENGTH * (level - 1);
        int dexterity = BASE_DEXTERITY + SCALING_DEXTERITY * (level - 1);
        int intelligence = BASE_INTELLIGENCE + SCALING_INTELLIGENCE * (level - 1);
        return new Attributes(health, strength, dexterity, intelligence);
    }
}
