package com.experis.rthode.models.characters.characterclasses;

import com.experis.rthode.models.Attributes;

/**
 * Representation of the mage class.
 */
public class Ranger implements CharacterClass {
    private final int BASE_HEALTH = 120;
    private final int BASE_STRENGTH = 5;
    private final int BASE_DEXTERITY = 10;
    private final int BASE_INTELLIGENCE = 2;

    private final int SCALING_HEALTH = 20;
    private final int SCALING_STRENGTH = 2;
    private final int SCALING_DEXTERITY = 5;
    private final int SCALING_INTELLIGENCE = 1;

    public Attributes calculateBaseAttributes(int level) {
        int health = BASE_HEALTH + SCALING_HEALTH * (level - 1);
        int strength = BASE_STRENGTH + SCALING_STRENGTH * (level - 1);
        int dexterity = BASE_DEXTERITY + SCALING_DEXTERITY * (level - 1);
        int intelligence = BASE_INTELLIGENCE + SCALING_INTELLIGENCE * (level - 1);
        return new Attributes(health, strength, dexterity, intelligence);
    }
}
