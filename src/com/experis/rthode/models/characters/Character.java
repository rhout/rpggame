package com.experis.rthode.models.characters;

import com.experis.rthode.models.Inventory;
import com.experis.rthode.models.Level;
import com.experis.rthode.models.Attributes;
import com.experis.rthode.models.characters.characterclasses.CharacterClass;
import com.experis.rthode.models.items.Item;

/**
 * Contains all the data for the character in the game.
 */
public class Character {
    String name;
    Level level = new Level();
    CharacterClass characterClass;
    Inventory inventory = new Inventory();

    public Character(String name, CharacterClass characterClass) {
        this.name = name;
        this.characterClass = characterClass;
    }

    /**
     * Adds the gained experience to the character current xp and updates the level accordingly.
     *
     * @param gainedXp
     */
    public void gainXp(int gainedXp) {
        level.gainXp(gainedXp);
    }

    /**
     * Equips the specified item in the slot where it belongs.
     * If an item is already in the slot, it will be overridden.
     *
     * @param item
     */
    public void equipItem(Item item) {
        if (canEquip(item)) {
            inventory.equip(item);
        }
    }

    /**
     * Determines if an item can be equipped, which is when the character level is at least the item level.
     *
     * @param item to be equipped
     * @return
     */
    public boolean canEquip(Item item) {
        return level.getLevel() >= item.getItemLevel();
    }

    /**
     * Makes an attack with the character. An attack can only happen if a weapon is equipped.
     * The attavk damage is calculated with all the equipped items.
     *
     * @return The attack damage
     */
    public int attack() {
        if (!canAttack()) {
            return 0;
        }
        Attributes totalAttributes = new Attributes(calculateBaseAttributes());
        totalAttributes.add(calculateBonusAttributes());
        return inventory.getEquippedWeapon().calculateDamageBasedOnAttributes(totalAttributes);
    }

    /**
     * Checks if a weapon is equipped.
     *
     * @return Whether a weapon is equipped or not.
     */
    public boolean canAttack() {
        return inventory.getEquippedWeapon() != null;
    }

    /**
     * Calculates how much xp is needed to gain the next level.
     *
     * @return
     */
    public int getXpToNextLevel() {
        return level.xpToNextLevel();
    }

    public int getCurrentXp() {
        return level.getCurrentXp();
    }

    public int getLevel() {
        return level.getLevel();
    }

    public String getName() {
        return name;
    }

    /**
     * Gets the type/class of the character.
     *
     * @return
     */
    public CharacterClass getCharacterClass() {
        return characterClass;
    }

    /**
     * CAlculates the attributes based solely on the base stats and the level of the character.
     *
     * @return
     */
    public Attributes calculateBaseAttributes() {
        return characterClass.calculateBaseAttributes(getLevel());
    }

    /**
     * Calculates the attributes gained from all equipped armor.
     *
     * @return
     */
    public Attributes calculateBonusAttributes() {
        return inventory.calculateAllArmorBonuses();
    }
}
