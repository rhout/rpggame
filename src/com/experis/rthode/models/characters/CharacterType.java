package com.experis.rthode.models.characters;

/**
 * The types of characters available in the game.
 */
public enum CharacterType {
    WARRIOR,
    RANGER,
    MAGE
}
