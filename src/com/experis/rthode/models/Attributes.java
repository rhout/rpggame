package com.experis.rthode.models;

/**
 * A pure data class (POJO) containing the attributes data.
 */
public class Attributes {
    public int health;
    public int strength;
    public int dexterity;
    public int intelligence;

    /**
     * Creates the attributes object with all attributes set to zero.
     */
    public Attributes() {
        this.health = 0;
        this.strength = 0;
        this.dexterity = 0;
        this.intelligence = 0;
    }

    /**
     * A clone constructor, that creates a new object with the same values as the provided one.
     * @param clone
     */
    public Attributes(Attributes clone) {
        this.health = clone.health;
        this.strength = clone.strength;
        this.dexterity = clone.dexterity;
        this.intelligence = clone.intelligence;
    }

    /**
     * Creates an attributes object where all the attributes are specified.
     * @param health
     * @param strength
     * @param dexterity
     * @param intelligence
     */
    public Attributes(int health, int strength, int dexterity, int intelligence) {
        this.health = health;
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    /**
     * Adds the given attributes to this attributes instance.
     * @param attributesToAdd
     */
    public void add(Attributes attributesToAdd) {
        this.health += attributesToAdd.health;
        this.strength += attributesToAdd.strength;
        this.dexterity += attributesToAdd.dexterity;
        this.intelligence += attributesToAdd.intelligence;
    }
}
