# :star2: The great 2020 RPG game :star2:
A small application simulating an RPG game with different character classes, equipment, leveling and attacking.

The application is a task on design patterns and decisions like composition over inheritance, S.O.L.I.D. and the strategy pattern.

There is no game flow, but the application is validated through unit tests. 
### Classes
A character has different attributes based on the class. The classes are: 
 * Warrior :muscle:
 * Mage :sparkles:
 * Ranger :arrow_upper_right:
 
 ### Items :gun: 
 The game contains weapons and armors.
 **Armors** can be of type *cloth*, *leather* and *plate*. Furthermore an armor is assigned to a slot: *torso*, *head* and *legs*.
 A **weapon** goes to the hand and can be of the types *melee*, *ranged* and *magic*.
 
 Both armors and weapons have different stats based on their types.